var Vehicle;
(function (Vehicle) {
    var Car = (function () {
        function Car(make, model) {
            this.make = make;
            this.model = model;
        }
        return Car;
    }());
    Vehicle.Car = Car;
    var audiCar = new Car("Audi", "Q7");
    console.log(audiCar);
})(Vehicle || (Vehicle = {}));
// This works now
var fordCar = new Vehicle.Car("Ford", "Figo");
//console.log(audiCar);
console.log(fordCar);
