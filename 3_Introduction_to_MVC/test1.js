/**
 * Created by mahedi on 16/05/2017.
 */
var Logger = (function () {
    var loggerInstance;
    var createLogger = function () {
        var _logWarning = function (message) {
            return message.toUpperCase();
        };
        return {
            logWarning: _logWarning
        };
    };
    return {
        getInstance: function () {
            if (!loggerInstance) {
                loggerInstance = createLogger();
            }
            return loggerInstance;
        }
    };
})();

var myLogger = Logger.getInstance();
myLogger.logWarning("warning");