var Vehicle;
(function (Vehicle) {
    var Car = (function () {
        function Car(make, model) {
            this.make = make;
            this.model = model;
        }
        return Car;
    }());
    var audiCar = new Car("Audi", "Q7");
})(Vehicle || (Vehicle = {}));
// This won't work
var fordCar = new Vehicle.Car("Ford", "Figo");
