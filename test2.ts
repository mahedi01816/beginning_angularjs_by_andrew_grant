module Vehicle {
    class Car {
        constructor(public make: string,
                    public model: string) {
        }
    }
    var audiCar = new Car("Audi", "Q7");
}
// This won't work
var fordCar = new Vehicle.Car("Ford", "Figo");