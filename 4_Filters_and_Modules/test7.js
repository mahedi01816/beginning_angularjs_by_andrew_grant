/**
 * Created by mahedi on 26/05/2017.
 */
function toTitleCase(str)
{
    return str.charAt(0).toUpperCase() + str.substr(1).toLowerCase();
}
console.log(toTitleCase("jennifer"));
console.log(toTitleCase("jENniFEr"));
console.log(toTitleCase("jENniFEr amanda Grant"));