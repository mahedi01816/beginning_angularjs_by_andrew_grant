/**
 * Created by mahedi on 16/05/2017.
 */
function MyFilterDemoCtrl($scope) {
    var someData = {
        firstName: "ABC",
        surname: "DEF",
        dateJoined: new Date(2010, 5, 3),
        consumption: 123.1458,
        plan: "super-basic-plan"
    };
    someData.firstName = someData.firstName.toLowerCase();
    someData.surname = someData.surname.toLowerCase();
    $scope.data = someData;
}
