/**
 * Created by mahedi on 26/05/2017.
 */
var myAppModule = angular.module('myAppModule', []);

myAppModule.filter('stripDashes', function() {
    return function(txt) {
        return textToFilter.split('-').join(' ');
    };
});

myAppModule.filter("toTitleCase", function () {
    return function (str) {
        return str.replace(/\w\S*/g, function(txt){ return txt.charAt(0).toUpperCase() + txt.
            substr(1).toLowerCase();});
    };
});

myAppModule.controller('MyFilterDemoCtrl', function ($scope) {

    }
);