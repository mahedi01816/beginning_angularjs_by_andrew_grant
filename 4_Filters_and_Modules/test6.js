/**
 * Created by mahedi on 26/05/2017.
 */
function stripDashes(txt) {
    return txt.split('-').join(' ');
};
console.log(stripDashes("super-basic-plan"));
console.log(stripDashes("something-with-a-lot-more-dashes-plan"));
console.log(stripDashes("noDashesPlan"));