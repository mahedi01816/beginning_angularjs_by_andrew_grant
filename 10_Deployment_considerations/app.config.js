angular.module('app.config', [])
    .constant('myConfig', {
        'apiUrl': 'http://localhost:8080',
        'adminEmail': 'admin@mycompany.com'
    });