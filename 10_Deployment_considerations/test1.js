module.factory('memberDataStoreService', function ($http, myConfig) {
    var memberDataStore = {};
    memberDataStore.doRegistration = function (theData) {
        var promise = $http({method: 'POST', url: myConfig.apiUrl, data: theData});
        return promise;
    };
    return memberDataStore;
});