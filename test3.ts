module Vehicle {
    export class Car {
        constructor(public make: string,
                    public model: string) {
        }
    }
    var audiCar = new Car("Audi", "Q7");
    console.log(audiCar);
}
// This works now
var fordCar = new Vehicle.Car("Ford", "Figo");

//console.log(audiCar);
console.log(fordCar);