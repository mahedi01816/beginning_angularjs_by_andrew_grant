var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Animal = (function () {
    function Animal(name) {
        this.name = name;
    }
    return Animal;
}());
var Cat = (function (_super) {
    __extends(Cat, _super);
    function Cat(name, domestic) {
        var _this = _super.call(this, name) || this;
        _this.domestic = true;
        return _this;
    }
    return Cat;
}(Animal));
var Tiger = (function (_super) {
    __extends(Tiger, _super);
    function Tiger(name, domestic) {
        var _this = _super.call(this, name) || this;
        _this.domestic = false;
        return _this;
    }
    return Tiger;
}(Animal));
