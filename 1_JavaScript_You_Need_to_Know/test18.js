/**
 * Created by mahedi on 15/05/2017.
 */
var userIsLoggedIn = false;
var userIsVIP = true;
if(userIsLoggedIn){
    console.log("Welcome back - showing you some very private data");
    if(userIsVIP){
        console.log("You are entitled to a 25% discount!");
    }else{
        console.log("You are entitled to a 10% discount!");
    }
}