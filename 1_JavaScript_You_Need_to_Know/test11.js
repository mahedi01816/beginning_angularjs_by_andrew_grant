/**
 * Created by mahedi on 15/05/2017.
 */
var myFirstObject = {};
myFirstObject.firstName = "mahedi";
myFirstObject.lastName = "hasan";
console.log(myFirstObject.firstName);

var mySecondObject = {
    firstName: "mahedi",
    lastName: "hasan",
};
console.log(mySecondObject.lastName);

var myThirdObject = new Object();
myThirdObject.firstName = "test";
myThirdObject.lastName = "me";
console.log(myThirdObject.firstName);