/**
 * Created by mahedi on 15/05/2017.
 */
var myCleverObject = {
    firstName: "a",
    age: 2,
    myInfo: function () {
        console.log("name " + this.firstName);
        console.log("age " + this.age);
    },
};
myCleverObject.myInfo();