class Animal {
    public domestic: boolean;

    constructor(public name: string) {
    }
}

class Cat extends Animal {
    constructor(name: string, domestic: boolean) {
        super(name);
        this.domestic = true;
    }
}

class Tiger extends Animal {
    constructor(name: string, domestic: boolean) {
        super(name);
        this.domestic = false;
    }
}